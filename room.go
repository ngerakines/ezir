package ezir

import (
	"fmt"
	"io"
)

// Room is neat.
type Room struct {
	Realm *Realm
	Zone  *Zone
	Name  string

	Description string

	Portals []*Portal
	Objects []*Object
}

// View is neat.
func (r *Room) View(w io.Writer) error {
	if _, err := fmt.Fprintf(w, "###\n%s ~ %s\n", r.Zone.Name, r.Name); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(w, "%s\n\n", r.Description); err != nil {
		return err
	}

	return nil
}
