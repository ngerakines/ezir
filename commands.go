package ezir

// Command is neat.
type Command interface {
	Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error
}

// Commands is neat.
type Commands map[string]Command

// InitCommands is neat.
func InitCommands(scene *Scene) (Commands, Command) {
	cmds := make(Commands)

	cmds["help"] = &HelpCommand{}
	cmds["info"] = &InfoCommand{}
	cmds["quit"] = &QuitCommand{}
	cmds["tick"] = &TickCommand{}

	// player commands
	cmds["inventory"] = &InventoryCommand{}

	// room, object, and creature commands
	cmds["view"] = &ViewCommand{}
	cmds["look"] = &LookCommand{}
	cmds["walk"] = &WalkCommand{}
	cmds["move"] = &GenericCommand{
		targetAction:    "move",
		noActionMessage: "You can't move that.",
		noArgMessage:    "Move what?",
	}
	cmds["take"] = &GenericCommand{
		targetAction:    "take",
		noActionMessage: "You can't take that.",
		noArgMessage:    "Take what?",
	}

	return cmds, &UnknownCommand{}
}
