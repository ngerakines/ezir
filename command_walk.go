package ezir

import (
	"fmt"
	"strings"
)

// WalkCommand is neat.
type WalkCommand struct {
}

const (
	defaultWalkFailureMessage = "You cannot walk in that direction."
)

var (
	directionAliases = map[string]string{
		"e":  "east",
		"n":  "north",
		"ne": "northeast",
		"nw": "northwest",
		"s":  "south",
		"se": "southeast",
		"sw": "southwest",
		"w":  "west",
	}
)

// Run is neat.
func (c *WalkCommand) Run(m *RealmManager, s *Scene, player *PlayerCharacter, args ...string) error {

	if len(args) < 2 {
		fmt.Fprintln(player.View, "Walk to where?")
		return nil
	}

	direction := strings.ToLower(args[1])
	if directionAlias, ok := directionAliases[direction]; ok {
		direction = directionAlias
	}

	portal := c.selectPortal(direction, s)
	if portal == nil {
		fmt.Fprintln(player.View, defaultWalkFailureMessage)
		return nil
	}

	for _, cond := range portal.Conditions {
		if !cond.Allow(m, s, player) {
			return nil
		}
	}

	destination := portal.Destination

	// Can we leave? Yes, for now.

	destinationZoneRooms, hasDestinationZoneRooms := m.Rooms[destination.ZoneName]
	if !hasDestinationZoneRooms {
		return fmt.Errorf("invalid destination: %s - %s", destination.ZoneName, destination.RoomName)
	}
	destinationRoom, hasDestinationRoom := destinationZoneRooms[destination.RoomName]
	if !hasDestinationRoom {
		return fmt.Errorf("invalid destination: %s - %s", destination.ZoneName, destination.RoomName)
	}
	destinationZoneScenes, hasZoneScenes := m.Scenes[destination.ZoneName]
	if !hasZoneScenes {
		return fmt.Errorf("invalid destination: %s - %s", destination.ZoneName, destination.RoomName)
	}

	destinationScene, hasDestinationScene := destinationZoneScenes[destination.RoomName]
	if !hasDestinationScene {
		destinationScene = &Scene{
			PlayerCharacters: make([]*PlayerCharacter, 0),
			Room:             destinationRoom,
		}
	}

	destinationScene.PlayerCharacters = append(destinationScene.PlayerCharacters, player)

	if err := destinationScene.PlayerEntersRoomEvent(player); err != nil {
		return err
	}
	if err := s.PlayerLeavesRoomEvent(player); err != nil {
		return err
	}

	destinationZoneScenes[destination.RoomName] = destinationScene
	m.Scenes[destination.ZoneName] = destinationZoneScenes
	m.PlayerScenes[player.Name] = destinationScene

	return nil
}

func (c *WalkCommand) selectPortal(direction string, s *Scene) *Portal {
	for _, portal := range s.Room.Portals {
		if portal.Action == "walk" && portal.Target == direction {
			return portal
		}
	}
	return nil
}
