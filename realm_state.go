package ezir

import (
	"encoding/csv"
	"fmt"
	"strings"
)

// RealmManager is neat.
type RealmManager struct {
	Realm          *Realm
	Zones          map[string]*Zone
	Rooms          map[string]map[string]*Room
	Scenes         map[string]map[string]*Scene
	Players        []*PlayerCharacter
	PlayerScenes   map[string]*Scene
	CreatureScenes map[string]*Scene
	Creatures      []*Creature
	Flags          map[string]struct{}
	Objects        map[string]*Object
	CreatureJobs   JobManager
}

var (
	// ErrNotInitialized is neat.
	ErrNotInitialized = fmt.Errorf("not initialized")

	// ErrZoneNotInitialized is neat.
	ErrZoneNotInitialized = fmt.Errorf("zone %w", ErrNotInitialized)

	// ErrRoomNotInitialized is neat.
	ErrRoomNotInitialized = fmt.Errorf("room %w", ErrNotInitialized)

	// ErrDoesNotExist is neat.
	ErrDoesNotExist = fmt.Errorf("does not exist")

	// ErrZoneDoesNotExist is neat.
	ErrZoneDoesNotExist = fmt.Errorf("zone %w", ErrDoesNotExist)

	// ErrRoomDoesNotExist is neat.
	ErrRoomDoesNotExist = fmt.Errorf("room %w", ErrDoesNotExist)
)

// NewRealmManager is neat.
func NewRealmManager(r *Realm) *RealmManager {
	return &RealmManager{
		Realm:          r,
		Zones:          make(map[string]*Zone),
		Scenes:         make(map[string]map[string]*Scene, 0),
		Rooms:          make(map[string]map[string]*Room, 0),
		Players:        make([]*PlayerCharacter, 0),
		PlayerScenes:   make(map[string]*Scene),
		CreatureScenes: make(map[string]*Scene),
		Creatures:      make([]*Creature, 0),
		Flags:          make(map[string]struct{}),
		Objects:        make(map[string]*Object),
		CreatureJobs:   NewJobManager(),
	}
}

// AddZone is neat.
func (m *RealmManager) AddZone(name string) *Zone {
	z := &Zone{
		Realm: m.Realm,
		Name:  name,
	}
	m.Zones[name] = z
	m.Rooms[name] = make(map[string]*Room)
	m.Scenes[name] = make(map[string]*Scene)

	return z
}

// AddRoom is neat.
func (m *RealmManager) AddRoom(zoneName, roomName, roomDescription string, portals []*Portal) error {
	zone, hasZone := m.Zones[zoneName]
	if !hasZone {
		return fmt.Errorf("zone does not exist: %s", zoneName)
	}
	zoneRooms, hasZoneRooms := m.Rooms[zoneName]
	if !hasZoneRooms {
		return fmt.Errorf("zone not initialized: %s", zoneName)
	}

	zoneRooms[roomName] = &Room{
		Realm:       m.Realm,
		Zone:        zone,
		Name:        roomName,
		Description: roomDescription,
		Portals:     portals,
	}
	m.Rooms[zoneName] = zoneRooms

	return nil
}

// SummonPlayer is neat.
func (m *RealmManager) SummonPlayer(player *PlayerCharacter, zoneName, roomName string) error {
	scene, err := m.getOrCreateScene(zoneName, roomName)
	if err != nil {
		return err
	}

	m.Players = append(m.Players, player)
	scene.PlayerCharacters = append(scene.PlayerCharacters, player)

	if err := scene.PlayerEntersRoomEvent(player); err != nil {
		return err
	}

	m.PlayerScenes[player.Name] = scene

	return nil
}

// PlayerCommand is neat.
func (m *RealmManager) PlayerCommand(input string, player *PlayerCharacter) error {

	scene, hasPlayerScene := m.PlayerScenes[player.Name]
	if !hasPlayerScene {
		return fmt.Errorf("player scene not initialized: %s", player.Name)
	}

	cmds, defaultCommand := InitCommands(scene)

	aliases := map[string]string{
		"e":         "walk east",
		"east":      "walk east",
		"n":         "walk north",
		"ne":        "walk northeast",
		"north":     "walk north",
		"northeast": "walk northeast",
		"northwest": "walk northwest",
		"nw":        "walk northwest",
		"s":         "walk south",
		"se":        "walk southeast",
		"south":     "walk south",
		"southeast": "walk southeast",
		"southwest": "walk southwest",
		"sw":        "walk southwest",
		"w":         "walk west",
		"west":      "walk west",
	}

	if aliasedCommand, ok := aliases[input]; ok {
		input = aliasedCommand
	}

	inputParts := strings.Fields(input)
	if len(inputParts) == 0 {
		return nil
	}

	r := csv.NewReader(strings.NewReader(input))
	r.Comma = ' ' // space
	fields, err := r.Read()
	if err != nil {
		return err
	}

	lowerName := strings.ToLower(fields[0])

	for name, cmd := range cmds {
		if lowerName == name {
			return cmd.Run(m, scene, player, fields...)
		}
	}

	return defaultCommand.Run(m, scene, player, fields...)
}

// Broadcast is neat.
func (m *RealmManager) Broadcast(message string) {
	for _, player := range m.Players {
		fmt.Fprintln(player.View, "[BROADCAST]", message)
	}
}

// RemoveFlags is neat.
func (m *RealmManager) RemoveFlags(flags ...string) {
	for _, flag := range flags {
		delete(m.Flags, flag)
	}
}

// AddFlags is neat.
func (m *RealmManager) AddFlags(flags ...string) {
	for _, flag := range flags {
		m.Flags[flag] = struct{}{}
	}
}

// AddObject is neat.
func (m *RealmManager) AddObject(zoneName, roomName, objectName, currentState string, objectStates map[string]ObjectState) error {
	_, hasZone := m.Zones[zoneName]
	if !hasZone {
		return fmt.Errorf("zone does not exist: %s", zoneName)
	}
	zoneRooms, hasZoneRooms := m.Rooms[zoneName]
	if !hasZoneRooms {
		return fmt.Errorf("zone not initialized: %s", zoneName)
	}
	room, hasRoom := zoneRooms[roomName]
	if !hasRoom {
		return fmt.Errorf("room not initialized: %s", zoneName)
	}

	objectKey := strings.Join([]string{zoneName, roomName, objectName}, ":")

	obj := &Object{
		Name:         objectName,
		CurrentState: currentState,
		States:       objectStates,
	}

	room.Objects = append(room.Objects, obj)

	m.Objects[objectKey] = obj

	return nil
}

// AddCreature is neat.
func (m *RealmManager) AddCreature(creature *Creature) error {
	scene, err := m.getOrCreateScene(creature.Start.ZoneName, creature.Start.RoomName)
	if err != nil {
		return err
	}

	m.Creatures = append(m.Creatures, creature)
	scene.Creatures = append(scene.Creatures, creature)
	m.CreatureScenes[creature.Name] = scene

	return nil
}

func (m *RealmManager) getOrCreateScene(zoneName, roomName string) (*Scene, error) {
	_, hasZone := m.Zones[zoneName]
	if !hasZone {
		return nil, fmt.Errorf("%w: %s", ErrZoneDoesNotExist, zoneName)
	}
	zoneRooms, hasZoneRooms := m.Rooms[zoneName]
	if !hasZoneRooms {
		return nil, fmt.Errorf("%w: %s", ErrZoneNotInitialized, zoneName)
	}

	zoneScenes, hasZoneScenes := m.Scenes[zoneName]
	if !hasZoneScenes {
		return nil, fmt.Errorf("%w: %s", ErrZoneNotInitialized, zoneName)
	}

	room, hasRoom := zoneRooms[roomName]
	if !hasRoom {
		return nil, fmt.Errorf("%w: %s", ErrRoomNotInitialized, zoneName)
	}

	scene, hasScene := zoneScenes[roomName]
	if !hasScene {
		scene = &Scene{
			PlayerCharacters: make([]*PlayerCharacter, 0),
			Room:             room,
		}
		m.Scenes[zoneName][roomName] = scene
	}

	return scene, nil
}

// GetCreatureScene is neat.
func (m *RealmManager) GetCreatureScene(creature *Creature) (*Scene, error) {
	scene, hasScene := m.CreatureScenes[creature.Name]
	if hasScene {
		return scene, nil
	}
	return nil, fmt.Errorf("no scene for creature")
}
