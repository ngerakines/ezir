package ezir

import (
	"fmt"
	"strings"
)

// ForEachString is neat.
func ForEachString(input []string, fn func(string) string) []string {
	results := make([]string, len(input))
	for i, value := range input {
		results[i] = fn(value)
	}
	return results
}

// LowerAllStrings is neat.
func LowerAllStrings(input []string) []string {
	return ForEachString(input, func(s string) string { return strings.ToLower(s) })
}

// QuoteAllStrings is neat.
func QuoteAllStrings(input []string) []string {
	return ForEachString(input, func(v string) string {
		return fmt.Sprintf("'%s'", v)
	})
}
