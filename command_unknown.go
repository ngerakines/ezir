package ezir

import (
	"fmt"
)

// UnknownCommand is neat.
type UnknownCommand struct {
}

// Run is neat.
func (c *UnknownCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	_, err := fmt.Fprintln(p.View, "Unknown command.")
	return err
}
