package ezir

import (
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
)

// Job is neat.
type Job interface {
	Run() error
	Shutdown(ctx context.Context) error
}

// JobManager is neat.
type JobManager interface {
	Add(j Job)
	Job
}

// jobManager is neat.
type jobManager struct {
	jobs map[uuid.UUID]Job

	ctx    context.Context
	cancel context.CancelFunc
	recv   chan jobState
}

type jobState struct {
	err error
	key uuid.UUID
}

// RunJob is neat.
func RunJob(job Job, maxShutdown time.Duration) (func() error, func(error)) {
	return func() error {
			return job.Run()
		},
		func(err error) {
			stopCtx, stopCancel := context.WithTimeout(context.Background(), maxShutdown)
			defer stopCancel()
			if err := job.Shutdown(stopCtx); err != nil {
				fmt.Println("error stopping job: ", err.Error())
			}
		}
}

// NewJobManager is neat.
func NewJobManager() JobManager {
	return &jobManager{
		jobs: make(map[uuid.UUID]Job),
		recv: make(chan jobState),
	}
}

// Add is neat.
func (jm *jobManager) Add(j Job) {
	key := uuid.Must(uuid.NewV4())
	jm.jobs[key] = j

	go func(j Job) {
		jm.recv <- jobState{err: j.Run(), key: key}
	}(j)

}

// Run is neat.
func (jm *jobManager) Run() error {
	jm.ctx, jm.cancel = context.WithCancel(context.Background())
	defer jm.cancel()

	for {
		select {
		case <-jm.ctx.Done():
			return jm.ctx.Err()

		case js := <-jm.recv:
			delete(jm.jobs, js.key)
			if js.err != nil {
				fmt.Println("job", js.key, "error:", js.err)
			}
		}
	}
}

// Shutdown is neat.
func (jm *jobManager) Shutdown(ctx context.Context) error {
	for _, job := range jm.jobs {
		if err := job.Shutdown(ctx); err != nil {
			fmt.Println("error stopping job: ", err.Error())
		}
	}

	jm.cancel()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-jm.ctx.Done():
		return jm.ctx.Err()
	}
}
