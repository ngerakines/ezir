package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/oklog/run"
	"gitlab.com/ngerakines/ezir"
)

func main() {

	var realmName string
	flags := flag.NewFlagSet("realm", flag.ExitOnError)
	flags.StringVar(&realmName, "realm", "ashbellow", "The name of the realm.")

	err := flags.Parse(os.Args)
	if err != nil {
		panic(err)
	}

	realmManager := ezir.NewRealmManager(&ezir.Realm{Name: realmName})

	realmClock := ezir.NewRealmClock(realmManager)

	err = ezir.LoadZone(realmManager, "data/zone_tower.yaml")
	if err != nil {
		panic(err)
	}

	nick := &ezir.PlayerCharacter{
		Name: "Nick",
		View: ezir.NewView(),
	}

	// mason := &ezir.PlayerCharacter{
	// 	Name: "Mason",
	// 	View: ezir.NewView(),
	// }

	var g run.Group

	go func() {
		if err := realmManager.SummonPlayer(nick, "tower", "tower lawn"); err != nil {
			panic(err)
		}
	}()

	// go func() {
	// 	if err := realmManager.SummonPlayer(mason, "Forest", "Forest Path"); err != nil {
	// 		panic(err)
	// 	}
	// }()

	g.Add(func() error {
		s := bufio.NewScanner(os.Stdin)
		for s.Scan() {
			err := realmManager.PlayerCommand(s.Text(), nick)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Error:", err)
			}
		}
		return s.Err()
	}, func(err error) {
		fmt.Println("input buffer exit called: ", err)
	})

	g.Add(ezir.RunJob(realmClock, 10*time.Second))
	g.Add(ezir.RunJob(nick.View, 10*time.Second))

	g.Add(ezir.RunJob(realmManager.CreatureJobs, 10*time.Second))
	// ezir.RunJob(&g, mason.View, 10*time.Second)

	if err := g.Run(); err != nil {
		panic(err)
	}
}
