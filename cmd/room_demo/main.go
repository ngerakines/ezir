package main

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"github.com/oklog/run"
	"gitlab.com/ngerakines/ezir"
)

func main() {

	nick := &ezir.PlayerCharacter{
		Name: "Nick",
		View: ezir.NewView(),
	}

	// mason := &ezir.PlayerCharacter{
	// 	Name: "Mason",
	// 	View: ezir.NewView(),
	// }

	realmManager := ezir.NewRealmManager(&ezir.Realm{Name: "Dark Woods"})
	realmManager.AddZone("Forest")
	err := realmManager.AddRoom(
		"Forest",
		"Forest Path",
		"An overgrown forest path leads into a dark woods. The path begins to the north.",
		[]*ezir.Portal{
			{
				Action:        "walk",
				Target:        "north",
				Destination:   ezir.Destination{ZoneName: "Forest", RoomName: "Overgrown Forest Path"},
				ArriveMessage: ezir.DefaultPortalArrive,
				DepartMessage: ezir.DefaultPortalDepart,
				Conditions:    []ezir.Condition{},
			},
		},
	)
	if err != nil {
		panic(err)
	}
	err = realmManager.AddRoom(
		"Forest",
		"Overgrown Forest Path",
		"Dark woods surround this overgrown forest path. A bush has recently been trampled.",
		[]*ezir.Portal{
			{
				Action:        "walk",
				Target:        "south",
				Destination:   ezir.Destination{ZoneName: "Forest", RoomName: "Forest Path"},
				ArriveMessage: ezir.DefaultPortalArrive,
				DepartMessage: ezir.DefaultPortalDepart,
				Conditions:    []ezir.Condition{},
			},
			{
				Action:        "walk",
				Target:        "east",
				Destination:   ezir.Destination{ZoneName: "Forest", RoomName: "Dense Forest"},
				ArriveMessage: ezir.DefaultPortalArrive,
				DepartMessage: ezir.DefaultPortalDepart,
				Conditions: []ezir.Condition{
					ezir.NewFlagRequiredCondition(realmManager, "Dark Woods:Forest:Forest Path:bush moved", "", "The bush is in the way.", false),
				},
			},
		},
	)
	if err != nil {
		panic(err)
	}

	err = realmManager.AddRoom(
		"Forest",
		"Dense Forest",
		"The forest is very dense. To the west is the forest path through a trampled bush.",
		[]*ezir.Portal{
			{
				Action:        "walk",
				Target:        "west",
				Destination:   ezir.Destination{ZoneName: "Forest", RoomName: "Overgrown Forest Path"},
				ArriveMessage: ezir.DefaultPortalArrive,
				DepartMessage: ezir.DefaultPortalDepart,
				Conditions: []ezir.Condition{
					ezir.NewFlagRequiredCondition(realmManager, "Dark Woods:Forest:Forest Path:bush moved", "", "The bush is in the way.", false),
				},
			},
		},
	)
	if err != nil {
		panic(err)
	}

	realmManager.AddObject(
		"Forest",
		"Overgrown Forest Path",
		"bush",
		"",
		"",
		ezir.NewBasicPlayerAction(
			"view",
			ezir.NewAllowCondition(),
			ezir.NewRoomDisplaySideEffect(
				"The bush looks like it has recently been trampled and moved. Below it are footsteps going east.",
				"",
			),
		),
		ezir.NewBasicPlayerAction(
			"move",
			ezir.NewAllowCondition(),
			ezir.NewAddFlagsSideEffect(
				"Dark Woods:Forest:Forest Path:bush moved",
			),
			ezir.NewRoomDisplaySideEffect(
				"You move the bush out of the way.",
				"The bush is moved out of the way.",
			),
		),
	)

	realmManager.AddObject(
		"Forest",
		"Forest Path",
		"torch",
		"A brightly lit torch is on the ground.",
		"Dark Woods:Forest:Forest Path:torch taken",
		ezir.NewBasicPlayerAction(
			"view",
			ezir.NewFlagRequiredCondition(realmManager, "Dark Woods:Forest:Forest Path:torch taken", "", "", true),
			ezir.NewRoomDisplaySideEffect(
				"It burns brightly and looks hot.",
				"",
			),
		),
		ezir.NewBasicPlayerAction(
			"take",
			ezir.NewFlagRequiredCondition(realmManager, "Dark Woods:Forest:Forest Path:torch taken", "", "", true),
			ezir.NewAddFlagsSideEffect(
				"Dark Woods:Forest:Forest Path:torch taken",
			),
			ezir.NewAddInventorySideEffect(
				"You take the torch.",
				func() *ezir.Equipment {
					return &ezir.Equipment{
						Name: "Torch",
					}
				},
			),
		),
	)

	if err != nil {
		panic(err)
	}

	realmClock := ezir.NewRealmClock(realmManager)

	var g run.Group

	go func() {
		if err := realmManager.SummonPlayer(nick, "Forest", "Forest Path"); err != nil {
			panic(err)
		}
	}()

	// go func() {
	// 	if err := realmManager.SummonPlayer(mason, "Forest", "Forest Path"); err != nil {
	// 		panic(err)
	// 	}
	// }()

	g.Add(func() error {
		s := bufio.NewScanner(os.Stdin)
		for s.Scan() {
			err := realmManager.PlayerCommand(s.Text(), nick)
			if err != nil {
				fmt.Fprintln(os.Stderr, "Error:", err)
			}
		}
		return s.Err()
	}, func(err error) {
		fmt.Println("input buffer exit called: ", err)
	})

	g.Add(ezir.RunJob(realmClock, 10*time.Second))
	g.Add(ezir.RunJob(nick.View, 10*time.Second))
	// ezir.RunJob(&g, mason.View, 10*time.Second)

	if err := g.Run(); err != nil {
		panic(err)
	}
}
