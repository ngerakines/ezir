package ezir

import (
	"fmt"
	"time"
)

// TickCommand is neat.
type TickCommand struct {
}

// Run is neat.
func (c *TickCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	fmt.Fprintln(p.View, time.Now().Format(time.RFC1123))
	return nil
}
