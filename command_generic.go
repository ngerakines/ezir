package ezir

import (
	"fmt"
	"strconv"
	"strings"
)

// GenericCommand is neat.
type GenericCommand struct {
	targetAction       string
	noArgMessage       string
	noActionMessage    string
	condFailureMessage string
}

// Run is neat.
func (c *GenericCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	if len(args) < 2 {
		if len(c.noActionMessage) > 0 {
			fmt.Fprintln(p.View, c.noArgMessage)
		}
		return nil
	}

	target := strings.ToLower(args[1])

	targets, err := selectSceneTargets(m, s, p, target, c.targetAction)
	if err != nil {
		return err
	}

	selection := 0
	if len(targets) > 1 && len(args) == 3 {
		selection, err = strconv.Atoi(args[2])
		if err != nil {
			fmt.Fprintln(p.View, "Invalid selection: ", selection, ". Must be 0 - ", len(targets))
			return nil
		}
	}

	if len(targets) > 1 {
		fmt.Fprintln(p.View, "More than one target available:")
		for i, targetOpt := range targets {
			var to strings.Builder
			to.WriteString(strconv.FormatInt(int64(i), 10))
			to.WriteString(". '")
			to.WriteString(targetOpt.Targets[0])
			to.WriteString("'")
			if len(targetOpt.Targets) > 1 {
				to.WriteString("(")
				for it, alias := range targetOpt.Targets {
					if it > 1 {
						to.WriteString("'")
						to.WriteString(alias)
						to.WriteString("'")
					}
				}
				to.WriteString(")")
			}
			fmt.Fprintln(p.View, to.String())
		}
		return nil
	}
	if selection < 0 || selection >= len(targets) {
		fmt.Fprintf(p.View, "Invalid selection. %d is not within the range 0-%d.\n", selection, len(targets)-1)
		return nil
	}
	action := targets[selection].PlayerAction

	if action == nil {
		if len(c.noActionMessage) > 0 {
			fmt.Fprintln(p.View, c.noActionMessage)
		}
		return nil
	}

	for _, cond := range action.Conditions() {
		if !cond.Allow(m, s, p) {
			if len(c.noActionMessage) > 0 {
				fmt.Fprintln(p.View, c.condFailureMessage)
			}
			return nil
		}
	}

	for _, sideEffect := range action.SideEffects() {
		if err := sideEffect.Action(m, s, p); err != nil {
			return err
		}
	}

	return nil
}
