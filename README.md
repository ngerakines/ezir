# About

Ezir is a multi-realm multi-player text based game engine.

# TODO

* Realm loading
* Zone loading
* Room loading
* Weather
* Non-player characters
* Objects
* Player experience and attributes
* Localization
* Room attributes
* Active vs passive viewers
* View object vs view equipment
* View multiple objects
