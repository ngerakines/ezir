package ezir

// RealmClock is neat.
type RealmClock struct {
	j            *TickJob
	realmManager *RealmManager
	ticksPerHour int
	minorTick    int
	superTick    int
}

// NewRealmClock is neat.
func NewRealmClock(realmManager *RealmManager) Job {
	rc := &RealmClock{
		realmManager: realmManager,
		ticksPerHour: 5,
		minorTick:    0,
		superTick:    0,
	}

	rc.j = &TickJob{
		Start: rc.start,
		Tick:  rc.tick,
	}

	return rc.j
}

func (rc *RealmClock) start() error {
	rc.realmManager.Broadcast("The realm is full of life.")
	rc.realmManager.AddFlags(NightFlag)
	return nil
}

func (rc *RealmClock) tick() error {
	rc.minorTick = rc.minorTick + 1

	newSuperTick := rc.superTick

	if rc.minorTick > rc.ticksPerHour {
		rc.minorTick = 0
		newSuperTick = rc.superTick + 1
	}

	if rc.superTick != newSuperTick {
		switch newSuperTick {
		case 0: // midnight
			rc.realmManager.Broadcast("The night is quiet.")
		case 6: // sunrise
			rc.realmManager.Broadcast("The sun rises.")
			rc.realmManager.RemoveFlags(NightFlag)
			rc.realmManager.AddFlags(DayFlag)
		case 12:
			rc.realmManager.Broadcast("The day is long and bright.")
		case 15: // sunset
			rc.realmManager.Broadcast("The sun begins to set.")
			rc.realmManager.RemoveFlags(DayFlag)
			rc.realmManager.AddFlags(NightFlag)
		case 19: // evening
			rc.realmManager.Broadcast("The creatures of the night have come out.")
		}
	}

	rc.superTick = newSuperTick
	return nil
}
