package ezir

import (
	"context"
	"fmt"
	"io"
	"os"
)

// View is neat.
type View struct {
	ctx    context.Context
	cancel context.CancelFunc
	out    io.Writer
	ch     chan []byte

	Prefix string
}

// NewView is neat.
func NewView() *View {
	return &View{
		out: os.Stdout,
		ch:  make(chan []byte),
	}
}

func (v *View) Write(p []byte) (n int, err error) {
	b := make([]byte, len(p))
	copy(b, p)
	v.ch <- b
	return len(p), nil
}

// Run is neat
func (v *View) Run() error {
	v.ctx, v.cancel = context.WithCancel(context.Background())

	for {
		select {
		case <-v.ctx.Done():
			return v.ctx.Err()

		case payload, ok := <-v.ch:
			if !ok {
				return fmt.Errorf("payload input not ok")
			}
			if len(v.Prefix) > 0 {
				if _, err := v.out.Write([]byte(v.Prefix)); err != nil {
					return err
				}
			}
			if _, err := v.out.Write(payload); err != nil {
				return err
			}
		}
	}
}

// Shutdown is neat.
func (v *View) Shutdown(ctx context.Context) error {
	v.cancel()

	select {
	case <-ctx.Done():
		return ctx.Err()

	case <-v.ctx.Done():
		return v.ctx.Err()
	}
}
