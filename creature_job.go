package ezir

import "fmt"

type rambleCreatureJob struct {
	realmManager *RealmManager
	creature     *Creature

	current int
	max     int
	message string

	j *TickJob
}

// NewRambleCreatureJob is neat.
func NewRambleCreatureJob(realmManager *RealmManager, creature *Creature, max int, message string) Job {
	cj := &rambleCreatureJob{
		realmManager: realmManager,
		creature:     creature,
		max:          max,
		message:      message,
	}

	cj.j = &TickJob{
		Start: cj.start,
		Tick:  cj.tick,
	}

	return cj.j
}

func (cj *rambleCreatureJob) start() error {
	scene, err := cj.realmManager.GetCreatureScene(cj.creature)
	if err != nil {
		return nil
	}
	for _, pc := range scene.PlayerCharacters {
		fmt.Fprintln(pc.View, "Who? What? Where am I?")
	}
	return nil
}

func (cj *rambleCreatureJob) tick() error {
	cj.current = cj.current + 1

	if cj.current > cj.max {
		cj.current = 0

		scene, err := cj.realmManager.GetCreatureScene(cj.creature)
		if err != nil {
			return nil
		}
		for _, pc := range scene.PlayerCharacters {
			fmt.Fprintln(pc.View, cj.message)
		}

	}
	return nil
}
