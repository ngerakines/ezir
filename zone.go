package ezir

// Zone is neat.
type Zone struct {
	Realm *Realm
	Name  string
}

// Destination is neat.
type Destination struct {
	ZoneName string
	RoomName string
}
