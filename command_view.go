package ezir

import (
	"fmt"
	"strconv"
	"strings"
)

// ViewCommand is neat.
type ViewCommand struct {
}

type viewTarget interface {
	GetName() string
	GetConditions() []Condition
	GetSideEffects() []SideEffect
}

// Run is neat.
func (c *ViewCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	if len(args) < 2 {
		if err := s.Room.View(p.View); err != nil {
			return err
		}
		for _, obj := range s.Room.Objects {
			if view, hasView := obj.GetRoomView(); hasView {
				if _, err := fmt.Fprintf(p.View, "%s\n", view); err != nil {
					return err
				}
			}
		}

		for _, creature := range s.Creatures {
			if len(creature.RoomDescription) > 0 && creature.RoomDescriptionCondition.Allow(m, s, p) {
				if _, err := fmt.Fprintf(p.View, "%s\n", creature.RoomDescription); err != nil {
					return err
				}
			}
		}

		return nil
	}

	targets, err := selectSceneTargets(m, s, p, strings.ToLower(args[1]), "view")
	if err != nil {
		return err
	}

	selection := 0

	if len(targets) > 1 {
		if len(args) == 3 {
			selection, err = strconv.Atoi(args[2])
			if err != nil {
				fmt.Fprintf(p.View, "Invalid selection. Value must be in range 0-%d.\n", len(targets)-1)
				return nil
			}
		}

		if selection < 0 || selection >= len(targets) {
			fmt.Fprintf(p.View, "Invalid selection. %d is not within the range 0-%d.\n", selection, len(targets)-1)
			return nil
		}

		if len(args) < 3 {
			fmt.Fprintln(p.View, "More than one target available:")
			for i, targetOpt := range targets {

				var to strings.Builder
				to.WriteString(fmt.Sprintf("%d. %s", i, targetOpt.Targets[0]))
				if len(targetOpt.Targets) > 1 {
					to.WriteString(" (")
					to.WriteString(strings.Join(QuoteAllStrings(targetOpt.Targets[1:]), ", "))
					to.WriteString(")")
				}
				fmt.Fprintln(p.View, to.String())
			}
			return nil
		}
	}

	if len(targets) == 0 {
		fmt.Fprintln(p.View, "There is nothing to look at.")
		return nil
	}

	action := targets[selection].PlayerAction

	if action == nil {
		fmt.Fprintln(p.View, "There is nothing to look at.")
		return nil
	}

	for _, cond := range action.Conditions() {
		if !cond.Allow(m, s, p) {
			fmt.Fprintln(p.View, "There is nothing to look at.")
			return nil
		}
	}

	for _, sideEffect := range action.SideEffects() {
		if err := sideEffect.Action(m, s, p); err != nil {
			return err
		}
	}

	return nil
}
