package ezir

import (
	"fmt"
	"io/ioutil"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

type zoneFileJob struct {
	Name        string `yaml:"name"`
	PrintPlayer string `yaml:"player_print"`
}

type zoneFileAction struct {
	Action      string `yaml:"action"`
	PrintPlayer string `yaml:"print_player"`
}

type zoneFileCreature struct {
	Name            string           `yaml:"name"`
	StartRoom       string           `yaml:"start"`
	RoomDescription string           `yaml:"room_view_description"`
	Actions         []zoneFileAction `yaml:"actions"`
	Jobs            []zoneFileJob    `yaml:"jobs"`
}

type zoneFileObjectState struct {
	GroundDescription string           `yaml:"ground_description"`
	RoomViewUnless    string           `yaml:"room_view_unless"`
	Aliases           []string         `yaml:"aliases"`
	Actions           []zoneFileAction `yaml:"actions"`
}

type zoneFileObject struct {
	Name         string                         `yaml:"name"`
	Room         string                         `yaml:"room"`
	Rooms        []string                       `yaml:"rooms"`
	CurrentState string                         `yaml:"current_state"`
	States       map[string]zoneFileObjectState `yaml:"states"`
}

type zoneFileRoom struct {
	Name        string            `yaml:"name"`
	Description string            `yaml:"description"`
	Portals     map[string]string `yaml:"portals"`
}

type zoneFile struct {
	Name      string             `yaml:"name"`
	Rooms     []zoneFileRoom     `yaml:"rooms"`
	Objects   []zoneFileObject   `yaml:"objects"`
	Creatures []zoneFileCreature `yaml:"creatures"`
}

// LoadZone is neat.
func LoadZone(realmManager *RealmManager, path string) error {

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var zf zoneFile

	err = yaml.Unmarshal(data, &zf)
	if err != nil {
		return err
	}

	zone := realmManager.AddZone(strings.ToLower(zf.Name))

	for _, room := range zf.Rooms {
		portals := make([]*Portal, 0)

		for direction, destination := range room.Portals {

			portals = append(portals, &Portal{
				Action:        "walk",
				Target:        strings.ToLower(direction),
				Destination:   parseDestinationStr(zone.Name, destination),
				ArriveMessage: DefaultPortalArrive,
				DepartMessage: DefaultPortalDepart,
				Conditions:    []Condition{},
			})
		}

		err = realmManager.AddRoom(
			strings.ToLower(zone.Name),
			strings.ToLower(room.Name),
			room.Description,
			portals,
		)
		if err != nil {
			return err
		}
	}

	for _, obj := range zf.Objects {

		objectStates := make(map[string]ObjectState)

		for name, zfos := range obj.States {

			var actions []PlayerAction

			for _, zfa := range zfos.Actions {
				oa, err := zfActionToPlayerAction(zfa)
				if err != nil {
					return err
				}
				actions = append(actions, oa)
			}

			objectStates[name] = ObjectState{
				Aliases:  zfos.Aliases,
				RoomView: zfos.GroundDescription,
				Actions:  actions,
				SceneModifier: SceneModifier{
					Flags: make([]string, 0),
				},
			}
		}

		if len(obj.Rooms) > 0 {
			for _, room := range obj.Rooms {
				err = realmManager.AddObject(
					strings.ToLower(zone.Name),
					strings.ToLower(room),
					strings.ToLower(obj.Name),
					obj.CurrentState,
					objectStates,
				)
			}

		} else if len(obj.Room) > 0 {
			err = realmManager.AddObject(
				strings.ToLower(zone.Name),
				strings.ToLower(obj.Room),
				strings.ToLower(obj.Name),
				obj.CurrentState,
				objectStates,
			)
		}
	}

	for _, zfc := range zf.Creatures {
		start := parseDestinationStr(zone.Name, zfc.StartRoom)

		var actions []PlayerAction

		for _, zfa := range zfc.Actions {
			oa, err := zfActionToPlayerAction(zfa)
			if err != nil {
				return err
			}
			actions = append(actions, oa)
		}

		creature := &Creature{
			Name:                     zfc.Name,
			Start:                    start,
			RoomDescription:          zfc.RoomDescription,
			RoomDescriptionCondition: NewAllowCondition(),
			Actions:                  actions,
		}

		err = realmManager.AddCreature(creature)
		if err != nil {
			return err
		}

		for _, job := range zfc.Jobs {
			if job.Name == "ramble" {
				realmManager.CreatureJobs.Add(NewRambleCreatureJob(realmManager, creature, 10, job.PrintPlayer))
			}
		}
	}

	return nil
}

func parseDestinationStr(zone, input string) Destination {
	if parts := strings.Split(input, ":"); len(parts) > 1 {
		return Destination{
			ZoneName: strings.ToLower(parts[0]),
			RoomName: strings.ToLower(parts[1]),
		}
	}
	return Destination{
		ZoneName: strings.ToLower(zone),
		RoomName: strings.ToLower(input),
	}
}

func zfActionToPlayerAction(zfa zoneFileAction) (PlayerAction, error) {
	switch zfa.Action {
	case "view", "move":
	default:
		return nil, fmt.Errorf("unsupported action: %s", zfa.Action)
	}

	var sideEffects []SideEffect

	if len(zfa.PrintPlayer) > 0 {
		sideEffects = append(sideEffects, NewRoomDisplaySideEffect(zfa.PrintPlayer, ""))

	}

	if len(sideEffects) == 0 {
		return nil, fmt.Errorf("invalid action: no side effects")
	}

	return NewBasicPlayerAction(
		strings.ToLower(zfa.Action),
		NewAllowCondition(),
		sideEffects...,
	), nil
}
