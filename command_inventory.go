package ezir

import (
	"fmt"
)

// InventoryCommand is neat.
type InventoryCommand struct {
}

// Run is neat.
func (c *InventoryCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	if len(p.Inventory) == 0 {
		fmt.Fprintln(p.View, "You have nothing in your inventory")
		return nil
	}

	for _, item := range p.Inventory {
		fmt.Fprintln(p.View, "*", item.Name)
	}

	return nil
}
