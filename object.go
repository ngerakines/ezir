package ezir

// Object contains information about things in rooms.
type Object struct {
	Name         string
	CurrentState string
	States       map[string]ObjectState
	// Actions                 []PlayerAction
	// GroundDescription       string
	// GroundDescriptionUnless string
}

// ObjectState is neat.
type ObjectState struct {
	Aliases       []string
	RoomView      string
	Actions       []PlayerAction
	SceneModifier SceneModifier
}

// SceneModifier is neat.
type SceneModifier struct {
	Flags []string
}

// PlayerActionTarget is neat.
type PlayerActionTarget struct {
	Targets      []string
	PlayerAction PlayerAction
}

/*
name: "torch"
current_state: "on_ground"
states:
  on_ground_lit:
	room_view: "A brightly lit torch is on the ground."
	player_actions:
	  get:
		- player_inventory torch
		- state destroyed
	  view:
	    - player "The torch glows brightly with a warm flame. It could be held safely."
	scene:
	  flags:
	    - lit
  on_wall:
	room_view: "A brightly lit torch is mounted on the wall."
	actions:
	  get:
		- player_inventory torch
		- state destroyed
	  view:
	    - player "The torch is mounted on the wall, but could be loosened and held safely."
	scene:
	  flags:
	    - lit
  destroyed:
    room_view: ""
*/

// GetRoomView is neat.
func (o *Object) GetRoomView() (string, bool) {
	objectState, ok := o.States[o.CurrentState]
	if !ok {
		return "", false
	}

	if len(objectState.RoomView) > 0 {
		return objectState.RoomView, true
	}
	return "", false
}

// AllTargets is neat.
func (o *Object) AllTargets() []string {
	results := []string{o.Name}
	objectState, ok := o.States[o.CurrentState]
	if ok && len(objectState.Aliases) > 0 {
		results = append(results, objectState.Aliases...)
	}
	return results
}

// MatchTarget is neat.
func (o *Object) MatchTarget(target string) bool {
	if o.Name == target {
		return true
	}
	objectState, ok := o.States[o.CurrentState]
	if !ok {
		return false
	}
	for _, alias := range objectState.Aliases {
		if alias == target {
			return true
		}
	}
	return false
}

// GetPlayerAction is neat.
func (o *Object) GetPlayerAction(targetAction string) PlayerAction {
	objectState, ok := o.States[o.CurrentState]
	if !ok {
		return nil
	}
	for _, action := range objectState.Actions {
		if action.Action() == targetAction {
			return action
		}
	}
	return nil
}

func selectSceneTargets(m *RealmManager, s *Scene, p *PlayerCharacter, target, targetAction string) ([]PlayerActionTarget, error) {

	// key := strings.Join([]string{s.Room.Zone.Name, s.Room.Name, target}, ":")

	// for _, e := range p.Inventory {
	// 	if e.Name == target {
	// 		for _, action := range e.Actions {
	// 			if action.Action() == targetAction {
	// 				return action, nil
	// 			}
	// 		}
	// 	}
	// }

	var playerActionTargets []PlayerActionTarget

	for _, roomObject := range s.Room.Objects {
		if roomObject.MatchTarget(target) {
			if pa := roomObject.GetPlayerAction(targetAction); pa != nil {
				playerActionTargets = append(playerActionTargets, PlayerActionTarget{
					Targets:      roomObject.AllTargets(),
					PlayerAction: pa,
				})
			}
		}
	}

	for _, creature := range s.Creatures {
		if creature.Name == target {
			for _, action := range creature.Actions {
				if action.Action() == targetAction {
					playerActionTargets = append(playerActionTargets, PlayerActionTarget{
						Targets:      []string{creature.Name},
						PlayerAction: action,
					})
				}
			}
		}
	}

	return playerActionTargets, nil
}

// func selectObject(m *RealmManager, zoneName, roomName, target, targetAction string) (PlayerAction, error) {
// 	key := strings.Join([]string{zoneName, roomName, target}, ":")

// 	obj, exists := m.Objects[key]
// 	if exists {
// 		for _, action := range obj.Actions {
// 			if action.Action() == targetAction {
// 				return action, nil
// 			}
// 		}
// 	}

// 	return nil, nil
// }
