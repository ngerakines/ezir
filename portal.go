package ezir

// Portal is neat.
type Portal struct {
	Action      string // "walk", "jump", "use"
	Target      string // "book", "portal", "north"
	Destination Destination

	ArriveMessage string
	DepartMessage string

	Conditions []Condition
}

const (
	// DefaultPortalArrive is neat.
	DefaultPortalArrive = "%s has arrived."

	// DefaultPortalDepart is neat.
	DefaultPortalDepart = "%s has departed."
)
