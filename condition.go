package ezir

import "fmt"

// Condition is neat.
type Condition interface {
	Allow(realmManager *RealmManager, scene *Scene, player *PlayerCharacter) bool
}

type flagRequiredCondition struct {
	realmManager *RealmManager
	flag         string
	failure      string
	success      string
	inverse      bool
}

// NewAllowCondition is neat.
func NewAllowCondition() Condition {
	return allowCondition{}
}

// NewFlagRequiredCondition is neat.
func NewFlagRequiredCondition(realmManager *RealmManager, flag, success, failure string, inverse bool) Condition {
	return &flagRequiredCondition{
		realmManager: realmManager,
		flag:         flag,
		inverse:      inverse,
		success:      success,
		failure:      failure,
	}
}

func (c *flagRequiredCondition) Allow(realmManager *RealmManager, scene *Scene, player *PlayerCharacter) bool {
	_, exists := c.realmManager.Flags[c.flag]
	if c.inverse {
		exists = !exists
	}

	if exists {
		if len(c.success) > 0 {
			fmt.Fprintln(player.View, c.success)
		}
		return true
	}

	if len(c.failure) > 0 {
		fmt.Fprintln(player.View, c.failure)
	}
	return false
}

type allowCondition struct{}

func (c allowCondition) Allow(*RealmManager, *Scene, *PlayerCharacter) bool {
	return true
}
