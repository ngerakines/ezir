package ezir

// PlayerAction is neat.
type PlayerAction interface {
	Action() string
	Conditions() []Condition

	SideEffects() []SideEffect
}

type basicPlayerAction struct {
	action      string
	cond        Condition
	sideEffects []SideEffect
}

// NewBasicPlayerAction is neat.
func NewBasicPlayerAction(action string, cond Condition, sideEffects ...SideEffect) PlayerAction {
	return &basicPlayerAction{
		action:      action,
		cond:        cond,
		sideEffects: sideEffects,
	}
}

func (boa *basicPlayerAction) Action() string {
	return boa.action
}

func (boa *basicPlayerAction) Conditions() []Condition {
	return []Condition{boa.cond}
}

func (boa *basicPlayerAction) SideEffects() []SideEffect {
	return boa.sideEffects
}
