package ezir

import "fmt"

// LookCommand is neat.
type LookCommand struct {
}

// Run is neat.
func (c *LookCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	if len(s.PlayerCharacters) == 1 {
		fmt.Fprintln(p.View, "There are no other players here.")
		return nil
	}
	for _, player := range s.PlayerCharacters {
		if player.Name != p.Name {
			fmt.Fprintf(p.View, "%s is here.", player.Name)
		}
	}
	return nil
}
