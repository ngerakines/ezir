package ezir

// Creature is neat.
type Creature struct {
	Name  string
	Start Destination

	RoomDescription          string
	RoomDescriptionCondition Condition

	Actions []PlayerAction
}

// CreatureActionCondition is neat.
type CreatureActionCondition interface {
	Allow(realmManager *RealmManager, scene *Scene, creature *Creature) bool
}

// CreatureActionSideEffect is neat.
type CreatureActionSideEffect interface {
	Action(realmManager *RealmManager, scene *Scene, creature *Creature) error
}

// CreatureAction is neat.
type CreatureAction interface {
	Conditions() []CreatureActionCondition

	SideEffects() []CreatureActionSideEffect
}
