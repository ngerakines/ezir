package ezir

import "fmt"

// SideEffect is neat.
type SideEffect interface {
	Action(realmManager *RealmManager, scene *Scene, player *PlayerCharacter) error
}

type addFlagsSideEffect struct {
	flags []string
}

type roomDisplaySideEffect struct {
	player  string
	viewers string
}

type addInventorySideEffect struct {
	player    string
	generator func() *Equipment
}

// NewAddFlagsSideEffect is neat.
func NewAddFlagsSideEffect(flags ...string) SideEffect {
	return &addFlagsSideEffect{
		flags: flags,
	}
}

// NewRoomDisplaySideEffect is neat.
func NewRoomDisplaySideEffect(player, viewers string) SideEffect {
	return &roomDisplaySideEffect{
		player:  player,
		viewers: viewers,
	}
}

// NewAddInventorySideEffect is neat.
func NewAddInventorySideEffect(player string, generator func() *Equipment) SideEffect {
	return &addInventorySideEffect{
		player:    player,
		generator: generator,
	}
}

func (se *addFlagsSideEffect) Action(realmManager *RealmManager, scene *Scene, player *PlayerCharacter) error {
	fmt.Println("[debug] addFlagsSideEffect action")

	realmManager.AddFlags(se.flags...)

	return nil
}

func (se *roomDisplaySideEffect) Action(_ *RealmManager, scene *Scene, player *PlayerCharacter) error {
	fmt.Println("[debug] roomDisplaySideEffect action")
	if len(se.player) > 0 {
		fmt.Fprintln(player.View, se.player)
	}
	if len(se.viewers) > 0 {
		for _, viewer := range scene.PlayerCharacters {
			if viewer.Name != player.Name {
				fmt.Fprintln(viewer.View, se.viewers)
			}
		}
	}

	return nil
}

func (se *addInventorySideEffect) Action(_ *RealmManager, scene *Scene, player *PlayerCharacter) error {
	fmt.Println("[debug] addInventorySideEffect action")
	if len(se.player) > 0 {
		fmt.Fprintln(player.View, se.player)
	}

	player.Inventory = append(player.Inventory, se.generator())

	return nil
}
