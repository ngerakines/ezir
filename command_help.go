package ezir

import (
	"fmt"
)

// HelpCommand is neat.
type HelpCommand struct {
}

// Run is neat.
func (c *HelpCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	fmt.Fprintln(p.View, "Commands:")
	fmt.Fprintln(p.View, " * help")
	fmt.Fprintln(p.View, " * info")
	fmt.Fprintln(p.View, " * quit")
	fmt.Fprintln(p.View, " * view")
	fmt.Fprintln(p.View, " * tick")
	fmt.Fprintln(p.View, " * look")
	fmt.Fprintln(p.View, " * Directions: north, south, east, and west")
	return nil
}
