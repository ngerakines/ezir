package ezir

import (
	"fmt"
)

// InfoCommand is neat.
type InfoCommand struct {
}

// Run is neat.
func (c *InfoCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	fmt.Fprintln(p.View, "Info!")
	return nil
}
