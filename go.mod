module gitlab.com/ngerakines/ezir

go 1.13

require (
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/kr/pretty v0.2.0
	github.com/oklog/run v1.1.0
	gopkg.in/yaml.v2 v2.3.0
)
