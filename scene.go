package ezir

import "fmt"

// Scene is neat.
type Scene struct {
	PlayerCharacters []*PlayerCharacter
	Creatures        []*Creature
	Room             *Room
}

// PlayerEntersRoomEvent is neat.
func (s *Scene) PlayerEntersRoomEvent(newPlayer *PlayerCharacter) error {
	if err := s.Room.View(newPlayer.View); err != nil {
		return err
	}
	for _, existingPlayer := range s.PlayerCharacters {
		if existingPlayer.Name != newPlayer.Name {
			fmt.Fprintln(existingPlayer.View, newPlayer.Name, "has arrived.")
		}
	}

	return nil
}

// PlayerLeavesRoomEvent is neat.
func (s *Scene) PlayerLeavesRoomEvent(newPlayer *PlayerCharacter) error {
	newPlayers := make([]*PlayerCharacter, 0)
	for _, existingPlayer := range s.PlayerCharacters {
		if existingPlayer.Name != newPlayer.Name {
			fmt.Fprintln(existingPlayer.View, newPlayer.Name, "has left.")
			newPlayers = append(newPlayers, existingPlayer)
		}
	}
	s.PlayerCharacters = newPlayers

	return nil
}
