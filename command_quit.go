package ezir

import (
	"fmt"
)

// QuitCommand is neat.
type QuitCommand struct {
}

// Run is neat.
func (c *QuitCommand) Run(m *RealmManager, s *Scene, p *PlayerCharacter, args ...string) error {
	fmt.Fprintln(p.View, "Exiting!")
	return fmt.Errorf("exiting")
}
