package ezir

import (
	"context"
	"errors"
	"fmt"
	"time"
)

// TickJob is neat.
type TickJob struct {
	ctx    context.Context
	cancel context.CancelFunc

	Start TickJobStart
	Tick  TickJobCallback
}

// TickJobStart is neat.
type TickJobStart func() error

// TickJobCallback is neat.
type TickJobCallback func() error

var (
	errHaltJob = fmt.Errorf("stop job")
)

// Run is neat.
func (j *TickJob) Run() error {
	j.ctx, j.cancel = context.WithCancel(context.Background())
	defer j.cancel()

	if j.Tick == nil {
		return fmt.Errorf("invalid tick job callback")
	}

	if j.Start != nil {
		if err := j.Start(); err != nil {
			return err
		}
	}

	for {
		select {
		case <-j.ctx.Done():
			return j.ctx.Err()

		case <-time.After(1 * time.Second):
			if err := j.Tick(); err != nil {
				if errors.Is(err, errHaltJob) {
					return nil
				}
				return err
			}
		}
	}
}

// Shutdown is neat.
func (j *TickJob) Shutdown(ctx context.Context) error {
	j.cancel()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-j.ctx.Done():
		return j.ctx.Err()
	}
}
